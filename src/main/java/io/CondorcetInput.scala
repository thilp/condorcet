package io

import condorcet.Rank

/**
 * Defines the input of this program.
 *
 * Other classes that parse specific file formats
 * or another kind of input must inherit from this class.
 */
abstract class CondorcetInput {
  val candidates: IndexedSeq
  def nextRanks: Option[IndexedSeq[Rank]]
  def hasNext: Boolean
}
