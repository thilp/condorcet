package io

import condorcet.{Rank, Ordering}
import java.io.File
import scala.io.Source

/**
 * Reads a CSV file and extracts its lines as ballots.
 * @param path where to find the CSV file.
 */
class CSVReader(val path: File) extends CondorcetInput {

  /** An iterator on the file's lines. */
  protected val fh = Source.fromFile(path, "UTF-8").getLines()

  /** List of candidates (whose names have been provided by the first CSV line) */
  val candidates = fh.next().split(',').tail

  /** Returns true if there is more ballots to be extracted, false otherwise. */
  def hasNext: Boolean = fh.hasNext

  /**
   * Returns the sorted sequence (in decreasing order) of the Ranks corresponding
   * to the next line, or None if there is no more lines.
   */
  def nextRanks: Option[IndexedSeq[Rank]] =
    if (fh.hasNext) {
      val scores = candidates zip fh.next().split(',').tail
      val ord = new Ordering[Int]
      for ((c, s) <- scores) ord += (c, s.toInt)
      Some(ord.toIndexedSeq)
    }
    else None
}
