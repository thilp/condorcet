package org.thilp.scala.io


/**
 * Defines the input of this program.
 *
 * Other classes that parse specific file formats
 * or another kind of input must inherit from this class.
 */
abstract class CondorcetInput {
  val candidates: Array[String]
  def nextRanks: Option[IndexedSeq[Rank]]
  def hasNext: Boolean
}
