package org.thilp.scala.condorcet

import java.io.{FileNotFoundException, File}
import org.thilp.scala.io.{CSVReader, CondorcetInput}

/**
 * Part of condorcet
 * Created on 07, 03 2013 at 12:43 AM.
 * Revision 1
 */

/** Contains the program's main methods. */
object Condorcet {

  /**
   * Processes data from a CSV file.
   * @param path path to the input CSV file.
   * @return an `IndexedSeq` of candidates, as specified in the CSV file's first column,
   *         sorted from greatest winner to greatest looser.
   */
  def processCSV(path: String): IndexedSeq[String] = {
    val file = new File(path)
    if (file.canRead && file.isFile) process(new CSVReader(file))
    else throw new FileNotFoundException(path + " does not represent a valid input file")
  }

  private def process(in: CondorcetInput): IndexedSeq[String] = {

    val pairs = new collection.mutable.HashMap[(String, String), Int]

    // Import all data
    while (in.hasNext) in.nextRanks match {
      case Some(seq) => for (IndexedSeq(a, b) <- seq.combinations(2); sa <- a.set; sb <- b.set)
        pairs((sa, sb)) = pairs.getOrElseUpdate((sa, sb), 0) + 1
      case None => throw new InternalError("CondorcetInput.hasNext lied")
    }

    // Simplify pairs
    ???
  }

}
