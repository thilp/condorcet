package org.thilp.scala.condorcet

/**
 * Manages the equality of different elements in an [[org.thilp.scala.condorcet.Ordering]].
 *
 * It keeps a set of tags for a given value, so that different elements (with different tags)
 * that share the same value are equal in the Ordering but their tag is kept.
 */
class Rank {
  val set = new collection.mutable.HashSet[String]
}
