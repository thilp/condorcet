package org.thilp.scala.condorcet

import scala.collection.JavaConversions.mapAsScalaMap
import scala.collection.mutable

/**
 * An external ordering relation (< or =) between some objects.
 * @tparam V value to be ordered
 */
class Ordering[V <% Ordered[V]](sizeHint: Int = 0) {

  /** An ordered list of ranks, implemented as a Red-Black tree based map. */
  private val val2rank = mapAsScalaMap(new java.util.TreeMap[V, Rank])
  if (sizeHint > 0) val2rank.sizeHint(sizeHint)

  /** Returns the value associated to a given tag. */
  private val tag2val = new mutable.HashMap[String, V]
  if (sizeHint > 0) tag2val.sizeHint(sizeHint)

  /**
   * Adds a new element in this ordered sequence.
   * @param value the value on which the ordering is applied.
   * @param tag a tag associated to the value
   * @return this Ordering with a new element
   */
  def add(tag: String, value: V): this.type =
    if ((tag2val contains tag) && tag2val(tag) == value) this
    else {

      if (tag2val contains tag) {
        // Cut the relationship between the old value and <tag>
        val oldval = tag2val(tag)
        val2rank(oldval).set -= tag
        if (val2rank(oldval).set.isEmpty) val2rank -= oldval
      }

      // Create the relationship between <value> and <tag>
      val2rank.getOrElseUpdate(value, new Rank).set += tag
      tag2val(tag) = value

      this
    }

  /** Adds a new element in this Ordering. See `add`. */
  def +=(t: (String, V)) = this.add(t._1, t._2)

  /**
   * Removes a tag from this ordering.
   * If no tag carries the associated value anymore, this value is removed too.
   * @param tag the tag to remove
   * @return this Ordering without `tag`
   */
  def remove(tag: String): this.type = {
    if (tag2val contains tag) {
      val v = tag2val(tag)
      val2rank(v).set -= tag
      if (val2rank(v).set.isEmpty) val2rank -= v
      tag2val -= tag
    }
    this
  }

  /** Removes the given `tag` from this Ordering. See `remove`. */
  def -=(tag: String) = this.remove(tag)

  /** Returns a [[scala.collection.IndexedSeq]] of the Ranks sorted from the best to worst rated. */
  def toIndexedSeq: IndexedSeq[Rank] = val2rank.values.toIndexedSeq.reverse
}
